mod utils;

use solve_sudoku9x9::Solver;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Params {
    #[wasm_bindgen(js_name = tryCount)]
    pub try_count: u32,

    #[wasm_bindgen(js_name = iterCount)]
    pub iter_count: u32,

    #[wasm_bindgen(js_name = coolingSpeed)]
    pub cooling_speed: f64,

    #[wasm_bindgen(js_name = randomSeed)]
    pub random_seed: u32,
}

#[wasm_bindgen]
impl Params {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            try_count: 200,
            iter_count: 10_000,
            cooling_speed: 0.8,
            random_seed: 5489,
        }
    }

    pub fn solve(&self, flatten_field: &mut [u32]) -> Option<String> {
        utils::set_panic_hook();
        if flatten_field.len() != 81 {
            return Some("flatten_fieldの長さが81ではない".to_string());
        }
        let mut field = flatten_field
            .chunks(9)
            .map(|row| row.to_vec())
            .collect::<Vec<_>>();
        let solver = match Solver::new(
            self.try_count,
            self.iter_count,
            self.cooling_speed,
            self.random_seed,
        ) {
            Ok(solver) => solver,
            Err(error) => return Some(error.to_string()),
        };
        match solver.solve(&mut field) {
            Ok(_) => {
                flatten_field.copy_from_slice(&field.concat());
                None
            }
            Err(error) => Some(error.to_string()),
        }
    }
}

impl Default for Params {
    fn default() -> Self {
        Self::new()
    }
}
