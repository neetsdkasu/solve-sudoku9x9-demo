# solve-sudoku9x9-demo

[solve-sudoku9x9](https://bitbucket.org/neetsdkasu/solve-sudoku9x9/)をブラウザ上(WebAssembly)で動かすデモ    


Demo Page: https://neetsdkasu.bitbucket.io/solve-sudoku9x9-demo/  

------------------------------------------------------

## 説明

うーん！全然解けないぞ！このプログラムは！！酷すぎる！！役立たず！！  

 - 人間にとって簡単と思える問題すら解けない場合もある…  
 - 人間にとって難しいと思える問題を解ける可能性は低いが解けることもごく稀にあるかもしれない…  


------------------------------------------------------

## その他

 - このプロジェクトsolve-sudoku9x9-demoは[wasm-pack](https://github.com/rustwasm/wasm-pack)を使用しました(`new`や`build`で生成されたファイルをこのリポジトリに含んでいます)
 - このプロジェクトsolve-sudoku9x9-demoの`Cargo.toml`に依存関係のCrateが書いてあります
