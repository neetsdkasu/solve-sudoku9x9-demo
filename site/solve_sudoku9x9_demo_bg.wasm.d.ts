/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_params_free(a: number): void;
export function __wbg_get_params_tryCount(a: number): number;
export function __wbg_set_params_tryCount(a: number, b: number): void;
export function __wbg_get_params_iterCount(a: number): number;
export function __wbg_set_params_iterCount(a: number, b: number): void;
export function __wbg_get_params_coolingSpeed(a: number): number;
export function __wbg_set_params_coolingSpeed(a: number, b: number): void;
export function __wbg_get_params_randomSeed(a: number): number;
export function __wbg_set_params_randomSeed(a: number, b: number): void;
export function params_new(): number;
export function params_solve(a: number, b: number, c: number, d: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
