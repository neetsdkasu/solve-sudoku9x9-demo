/* tslint:disable */
/* eslint-disable */
/**
*/
export class Params {
  free(): void;
/**
*/
  constructor();
/**
* @param {Uint32Array} flatten_field
* @returns {string | undefined}
*/
  solve(flatten_field: Uint32Array): string | undefined;
/**
*/
  coolingSpeed: number;
/**
*/
  iterCount: number;
/**
*/
  randomSeed: number;
/**
*/
  tryCount: number;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_params_free: (a: number) => void;
  readonly __wbg_get_params_tryCount: (a: number) => number;
  readonly __wbg_set_params_tryCount: (a: number, b: number) => void;
  readonly __wbg_get_params_iterCount: (a: number) => number;
  readonly __wbg_set_params_iterCount: (a: number, b: number) => void;
  readonly __wbg_get_params_coolingSpeed: (a: number) => number;
  readonly __wbg_set_params_coolingSpeed: (a: number, b: number) => void;
  readonly __wbg_get_params_randomSeed: (a: number) => number;
  readonly __wbg_set_params_randomSeed: (a: number, b: number) => void;
  readonly params_new: () => number;
  readonly params_solve: (a: number, b: number, c: number, d: number) => void;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
