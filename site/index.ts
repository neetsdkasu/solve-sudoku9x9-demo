import init, { Params } from "./solve_sudoku9x9_demo.js";

const field: Array<HTMLInputElement> =  new Array(81);

let params: Params | null = null;

let queryTryCount: number | null = null;
let queryIterCount: number | null = null;
let queryCoolingSpeed: number | null = null;
let queryRandomSeed: number | null = null;

function lock(disabled: boolean) {
    document.querySelectorAll("input")
        .forEach( e => e.disabled = disabled );
    document.querySelectorAll("button")
        .forEach( e => e.disabled = disabled );
}

function doSolve() {
    const msg = document.getElementById("message")!;
    if (params === null) {
        msg.classList.remove("success");
        msg.classList.add("error");
        msg.textContent = "UNKNOWN ERROR";
        return;
    }
    params.tryCount = queryTryCount ?? (Math.floor(Math.random() * 300) + 200);
    params.iterCount = queryIterCount ?? (Math.floor(Math.random() * 70000) + 10000);
    params.coolingSpeed = queryCoolingSpeed ?? (Math.random() * 0.6 + 0.4);
    params.randomSeed = queryRandomSeed ?? Math.floor(Math.random() * 1e9);
    try {
        const flattenField = new Uint32Array(81);
        for (let i = 0; i < field.length; i++) {
            if (!field[i].validity.valid) {
                throw "0～9以外の値が入力されている！";
            }
            let value = parseInt(field[i].value);
            if (isNaN(value)) {
                value = 0;
            }
            flattenField[i] = value;
        }
        const result = params.solve(flattenField);
        if (typeof result === "string") {
            msg.classList.remove("success");
            msg.classList.add("error");
            msg.textContent = result;
        } else {
            msg.classList.add("success");
            msg.classList.remove("error");
            msg.textContent = "COMPLETE!!";
            for (let i = 0; i < field.length; i++) {
                let value = parseInt(field[i].value);
                if (isNaN(value)) {
                    value = 0;
                }
                if (0 < value) {
                    field[i].classList.remove("filled");
                    field[i].classList.add("fixed");
                } else {
                    field[i].classList.remove("fixed");
                    field[i].classList.add("filled");
                }
                field[i].value = `${flattenField[i]}`;
            }
        }
    } catch (err) {
        msg.classList.remove("success");
        msg.classList.add("error");
        msg.textContent = `${err}`;
    } finally {
        lock(false);
    }
}

function solve() {
    const msg = document.getElementById("message")!;
    msg.classList.remove("success");
    msg.classList.remove("error");
    msg.textContent = "computing...";
    lock(true);
    window.setTimeout(doSolve, 1);
}

function clear() {
    const msg = document.getElementById("message")!;
    msg.classList.remove("success");
    msg.classList.remove("error");
    msg.textContent = "";
    for (const cell of field) {
        cell.value = "";
        cell.classList.remove("fixed");
        cell.classList.remove("filled");
    }
}

function initButtons() {
    const buttons = document.getElementById("buttons")!;
    const clearButton = buttons.appendChild(document.createElement("button"));
    clearButton.textContent = "CLEAR";
    clearButton.addEventListener("click", clear);
    const solveButton = buttons.appendChild(document.createElement("button"));
    solveButton.textContent = "SOLVE";
    solveButton.addEventListener("click", solve);
}

function initField() {
    const content = document.getElementById("content")!;
    const outerTable = content.appendChild(document.createElement("table"));
    for (let i = 0; i < 81; i++) {
        field[i] = document.createElement("input");
        field[i].type = "number";
        field[i].min = "0";
        field[i].max = "9";
    }
    for (let outerRow = 0; outerRow < 3; outerRow++) {
        const outerTr = outerTable.appendChild(document.createElement("tr"));
        for (let outerCol = 0; outerCol < 3; outerCol++) {
            const outerTd = outerTr.appendChild(document.createElement("td"));
            const innerTable = outerTd.appendChild(document.createElement("table"));
            for (let innerRow = 0; innerRow < 3; innerRow++) {
                const innerTr = innerTable.appendChild(document.createElement("tr"));
                for (let innerCol = 0; innerCol < 3; innerCol++) {
                    const innerTd = innerTr.appendChild(document.createElement("td"));
                    const fieldId = outerRow * 27 + outerCol * 3
                                    + innerRow * 9 + innerCol;
                    innerTd.appendChild(field[fieldId]);
                }
            }
        }
    }
}

function parseURLQueries() {
    const searchParams = new URLSearchParams(document.location.search);
    if (searchParams.has("tc")) {
        const tc = searchParams.get("tc")!;
        if (tc.match(/^\d{1,8}$/)) {
            queryTryCount = parseInt(tc);
            if (isNaN(queryTryCount) || queryTryCount < 1) {
                queryTryCount = null;
            }
        }
    }
    if (searchParams.has("ic")) {
        const ic = searchParams.get("ic")!;
        if (ic.match(/^\d{1,8}$/)) {
            queryIterCount = parseInt(ic);
            if (isNaN(queryIterCount) || queryIterCount < 1000) {
                queryIterCount = null;
            }
        }
    }
    if (searchParams.has("cs")) {
        const cs = searchParams.get("cs")!;
        if (cs.match(/^\d+$/)) {
            queryCoolingSpeed = parseFloat(`0.${cs}`);
            if (isNaN(queryCoolingSpeed) || !(0.0 < queryCoolingSpeed && queryCoolingSpeed < 1.0)) {
                queryCoolingSpeed = null;
            }
        }
    }
    if (searchParams.has("rs")) {
        const rs = searchParams.get("rs")!;
        if (rs.match(/^\d{1,8}$/)) {
            queryRandomSeed = parseInt(rs);
            if (isNaN(queryRandomSeed)) {
                queryRandomSeed = null;
            }
        }
    }
    if (searchParams.has("pr")) {
        const pr = searchParams.get("pr")!;
        if (pr.match(/^\d{81}$/)) {
            for (let i = 0; i < 81; i++) {
                const d = pr.charAt(i);
                if (d.match(/[1-9]/)) {
                    field[i].value = d;
                }
            }
        }
    }
}

init()
    .then( () => {
        initButtons();
        initField();
        params = new Params();
        parseURLQueries();
    })
    .catch( err => {
        const msg = document.getElementById("message")!;
        msg.classList.add("error");
        msg.textContent = `${err}`;
    });
